from Runge_Kutta import RKG
from Runge_Kutta import RKG_3D
import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D


if __name__ == '__main__':
    
    #Definimos la funcion para solucionar:

    f=lambda x,y: x-y
    
    #Inicializamos la clase:
    
    m=RKG(0.00001,4,[1/6,1/3,1/3,1/6],[0,1/2,1/2,1],[[1/2],[0,1/2],[0,0,1]],f)
    
    ##Gráfica de x-y con runge kutta:
    m.kutta_ploter(100000,0,0,"x-y_graf.png")

    # Error del Runge Kutta con la solucion analítica:
    m.kutta_compare(100000,0,0,"Error.png")

    #Atractor de lorentz solucionado con Runge Kutta 3D:
    #1:

    #Definimos los valores de sigma, Beta y alfa:
    beta=8/3
    roo=28.0
    sigma=10.0

    #Definimos las funciones:
    f1=lambda t,x: sigma*(x[1]-x[0])
    f2=lambda t,x: x[0]*(roo-x[2])-x[1]
    f3=lambda t,x: x[0]*x[1]-beta*x[2]

    #Inicializamos la clase de RKG_3D:
    c=RKG_3D(0.001,4,[1/6,1/3,1/3,1/6],[0,1/2,1/2,1],[[1/2],[0,1/2],[0,0,1]],f1,f2,f3)

    #Guardamos las gráficas:
    c.kutta_ploter_3D(1000000,0,[0.0,0.0,0.0],"lorentz1.png")

    #Graficas de x vs y, x vs z y y vs z:
    c.kutta_ploter_grafs(1000000,0,[0.0,0.0,0.0],"graphs_1")

    #2:

    #Definimos los valores de sigma, Beta y alfa:
    beta=8/3
    roo=28.0
    sigma=10.0

    #Definimos las funciones:
    f1=lambda t,x: sigma*(x[1]-x[0])
    f2=lambda t,x: x[0]*(roo-x[2])-x[1]
    f3=lambda t,x: x[0]*x[1]-beta*x[2]

    #Inicializamos la clase de RKG_3D:
    c=RKG_3D(0.001,4,[1/6,1/3,1/3,1/6],[0,1/2,1/2,1],[[1/2],[0,1/2],[0,0,1]],f1,f2,f3)

    #Guardamos las gráficas:
    c.kutta_ploter_3D(1000000,0,[0.0,0.01,0.0],"lorentz2.png")

    #Graficas de x vs y, x vs z y y vs z:
    c.kutta_ploter_grafs(1000000,0,[0.0,0.01,0.0],"graphs_2")

    #3:

     #Definimos los valores de sigma, Beta y alfa:
    beta=4
    roo=45
    sigma=16

    #Definimos las funciones:
    f1=lambda t,x: sigma*(x[1]-x[0])
    f2=lambda t,x: x[0]*(roo-x[2])-x[1]
    f3=lambda t,x: x[0]*x[1]-beta*x[2]

    #Inicializamos la clase de RKG_3D:
    c=RKG_3D(0.001,4,[1/6,1/3,1/3,1/6],[0,1/2,1/2,1],[[1/2],[0,1/2],[0,0,1]],f1,f2,f3)

    #Guardamos las gráficas:
    c.kutta_ploter_3D(1000000,0,[0.0,0.0,0.0],"lorentz3.png")

    #Graficas de x vs y, x vs z y y vs z:
    c.kutta_ploter_grafs(1000000,0,[0.0,0.0,0.0],"graphs_3")

    #4:


    #Definimos los valores de sigma, Beta y alfa:
    beta=4
    roo=45
    sigma=16

    #Definimos las funciones:
    f1=lambda t,x: sigma*(x[1]-x[0])
    f2=lambda t,x: x[0]*(roo-x[2])-x[1]
    f3=lambda t,x: x[0]*x[1]-beta*x[2]

    #Inicializamos la clase de RKG_3D:
    c=RKG_3D(0.001,4,[1/6,1/3,1/3,1/6],[0,1/2,1/2,1],[[1/2],[0,1/2],[0,0,1]],f1,f2,f3)

    #Guardamos las gráficas:
    c.kutta_ploter_3D(1000000,0,[0.0,0.001,0.0],"lorentz4.png")

    #Graficas de x vs y, x vs z y y vs z:
    c.kutta_ploter_grafs(1000000,0,[0.0,0.001,0.0],"graphs_4")
    
    
   